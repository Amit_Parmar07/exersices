console.log("Exercise");
if (jQuery) {
  console.log("Running");
}

$(document).ready(function () {
  $("#btn").on("click", function () {
    alert("jQuery is loaded");
  });

  $("a[href='#top']").click(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });

  $("#gbtm").click(function () {
    $("html, body").animate({ scrollTop: 9999 }, "slow");
    return false;
  });

  $("#img-container").on("contextmenu", function (e) {
    alert("Right click is disabled on this image");
    console.log(e);
    return false;
  });
  //4
  $("#chkbtn").on("click", function () {
    if ($("#sBtn").attr("disabled")) {
      $("#sBtn").removeAttr("disabled");
    } else {
      $("#sBtn").attr("disabled", "disabled");
    }
  });

  //6
  function blinking() {
    $("ul li").fadeOut(300);
    $("ul li").fadeIn(300);
  }
  setInterval(blinking(), 100);
  setInterval(blinking(), 100);
  setInterval(blinking(), 100);

  //   7

  $("tr:odd").css("background-color", "grey");

  //8

  $("#printBtn").on("click", function () {
    window.print();
  });

  // 9
  $("#textarea").on("keyup", function () {
    let max = 15;
    if ($(this).val().length < max) {
      $("#char").text(`${$(this).val().length} words`);
    } else {
      alert("Character limit exceded");
    }
  });

  // 10

  let a, b;
  $("#sentences p").each(function () {
    a = $(this).html();
    b = a.split(" ");
    $(this).html(`<b>${b[0]}</b> ${b[1]}`);
  });

  // 11 creating div using jquery

  $("#sentences").after(`<div id="jDiv">Created using div<div>`);

  //12 move div inside of another

  $("#move-btn").on("click", function () {
    $("#parent-one").append($("#child-one"));
  });

  // 13 selecting value from json object

  let jsonObj = `{"fname":"Amit","lname":"Parmar","city":"Bhavnagar"}`;

  let newData = JSON.parse(jsonObj);

  $("#printDetails").on("click", function () {
    $("#details").html(
      `Firstname is ${newData.fname} , Lastname ${newData.lname} and from ${newData.city}`
    );
  });

  //   14 adding list in unordered list

  $("#addListBtn").on("click", function () {
    $("#list-container ul").append("<li>new List</li<");
  });

  //   15 Remove all the options of a select box and then add one option and select it.

  $("#rmv-opt").on("click", function () {
    $("#mySelect").children().remove();
  });
  $("#add-opt").on("click", function () {
    $("#mySelect").append("<option>Cricket</option>");
  });

  // 16  Underline all the words of a text using jQuery.

  $("#underline").on("click", function () {
    let words = $("#words-container p").text();
    let newWords = words.split(" ");
    $("#words-container p").empty();
    $.each(newWords, function (index, value) {
      $("#words-container p").append(`<span>${value}</span> `);
      console.log(value);
    });
  });

  //   17 taking value from input box

  $("#textbox-container button").on("click", function () {
    let name = $("#inp-name").val();
    alert("Your name is : " + name);
  });

  //   18 Remove all classes using jQuery

  $("#rmv-styles").on("click", function () {
    $("#span-container span").removeClass();
  });

  // 19 Remove using .CSS()
  $("#div-container button").on("click", function () {
    $("#div-container .div-s").css("background-color", "white");
    $("#div-container .div-s span").css("text-decoration", "none");
  });

  //   20 difference between left and right click

  $("#click-detector").on("click", function () {
    obj = $(this).html("Mouse LEFT Button Clicked! ");
  });
  $("#click-detector").on("contextmenu", function () {
    $(this).html("Mouse RIGHT Button Clicked! ");
    return false;
  });

  //   21 check this is jQuery object or not
  let ab = {
    name: "Amit Parmar",
    city: "Bhavnagar",
  };
  let ba = $("#click-detector");

  if (ab.jquery) {
    console.log("ab is a jQuery object.");
  } else if (ba.jquery) {
    console.log("ba is a jQuery object.");
  } else {
    console.log("none of them are jQuery object");
  }


 //   22 check that if enter key pressed
 
    $("#input-container input").on("keydown",function(e){
        console.log(e)
        let enterk=e.keyCode;
        if(enterk==13){
            alert("Enter Key Pressed")
        }
        else{
            $("#input-container span").html("Key Preseed : "+e.key)
        }
    })

//  23 Count number of rows and columns in a table 
    let rows=0,cols=0;
    $("#table-container table th").each(function(){
        cols++;
    })
    $("#table-container table tr").each(function(){
        rows++;
    })

    $("#table-container #rows").html("Total Rows : "+rows);
    $("#table-container #cols").html("Total Columns : "+cols);

// 24  How to get textarea text using jQuery.
    $("#textarea-container button").on("click",function(){

        let Txtvalues=$("#TextArea").val();
       $("#textarea-container p").html(Txtvalues);
    })
 // 25 Accessing input fields.
    function display(dept){
        let name=$("#names").val();
        let gender=  $("#form-container input[type='radio']").val();
        console.log(name,gender,dept)
        $("#form-container p").html(`Name is ${name},${gender} from ${dept} Department ! `)
    }
    let dept="";
    $("#Dept").on("change",function(){
        dept=$(this).val();
       
    });
    $("#form-container form").on("submit",function(e){
        e.preventDefault();
        display(dept);
    })

 //  26 Absolute Position of element
    $("#div-a").on("click",function(){
       let spanTag= $("#div-a span").position();
        $("#div-a p").html(`LEFT : ${spanTag.left}  TOP : ${spanTag.top}`);
    })

//  27 convert jQuery object into string
    let h2={
        tagName:"h2",
        id:"heading",
        class:"heading-one"
    };

    let newh2=JSON.stringify(h2);
    console.log(newh2)

    $("#jQ-obj").append(`<${h2.tagName} id=${h2.id} class=${h2.class} >this is h2 tag </${h2.tagname}>`);

// 28 detect change when new word added in the text box

    $("#textb-container input").on("input",function(e){
        console.log("content changed !")
    })

// 29 Removing specific value from array

    let arr=["Amit","Tejasvi","Dhara","Vaishali","Ditya"];
    console.log(arr)
    let newarr=$.grep(arr,function(el,index){
       return el!="Tejasvi"
    })
    console.log(newarr)

// 30 changing button text
    $("#btn-container").on("click",function(){
        $(this).html("Submit");
    })

// 31 add option in dropdown
    let count=0;
   $("#addOps").on("click",function(){
       $("#addOp").append(`<option>${count++}</option>`);
   })

// 32 change backgroud color
   $("#chngbg").on("click",function(){
       
     $("#chng-img").css("background-image","url(https://picsum.photos/350/350/?blur=2)");
   })

// 33 delete rows except first

   $("#delBtn").on("click",function(){
   
    $("#tableR-container tr").each(function(){
      
        if($(this).index()!==0){
            $(this).remove();
            
        } 
      
    })
   })

// 34 getting currently selectedd values from the dropdown

   $("#mySports").on("change",function(){
       $("#dropD-container p").html(`Selected Sport : ${$(this).val()}`)
   })

// 35 Disable the link using the jQuery

   $("#disLink").on("click",function(){
       $("#links-container a").removeAttr("href");
   })
// 36 chnage css class using jQuery

   $("#chngClass").on("click",function(){
     
   $("#chgCls").removeClass("chgcls1").addClass("chgcls2");
     
   })

// 37 add class using jquery
   $("#desingMe").on("click",function(){
    $(this).addClass("dsgnBtn");
   })

// 38 count the number of <p> element
   let pCounter=0;
   $("#counterP").on("click",function(){
     $("#p-container").children("p").each(function(){
      pCounter++;
     })
     $("#p-container span").html(`Total Colours ${pCounter}`)
   })

// 39 Restrict "number"-only input for textboxes including decimal points.
   $("#rstr-inp-container input").on("keyup",function(){
    //  if(typeof($(this).val)!=Number){
    //     alert("Entered Value is not an number")
    //     $(this).val("")
    //  }


   })
// 40 currently loaded jquery version
   $("#jquery-v").on("click",function(){
     alert(`currently loaded version ${jQuery().jquery}`)
   })

// 41 deleting specific row

 $("#rmv-row1").on("click",function(){
   $("#rmv-specific-r #1").remove();
 })
 $("#rmv-row2").on("click",function(){
   $("#rmv-specific-r #2").remove();
 })
 $("#rmv-row3").on("click",function(){
   $("#rmv-specific-r #3").remove();
 })

//  42 set input using jquery
 $("#setinpBtn").on("click",function(){
   $("#set-inp").val("Hello")
 })

//  43 set span value using jquery
 $("#setSpanBtn").on("click",function(){
   $("#set-span").html("Hello i am Span Tag")
 })

//  44 get class of currently clicked element

 $("#main-divs div").each(function(){
   $(this).on("click",function(){
     alert(`Classname : ${$(this).attr("class")}`)
   })
 })
//  45 set the href attribute
 $("#set-link").on("click",function(){
   $("#set-href").attr("href","https://www.youtube.com/")
 })

//  46 remove the disabled attribute
 $("#enableInp").on("click",function(){
   $("#inputDis").removeAttr("disabled")
 })

//  47 find total width of element
 $("#divCalc").on("click",function(){
   let myDiv=$("#myDiv")
   $("#div-cal").html("Total Width :"+myDiv.outerWidth()+" Total Height :"+myDiv.outerHeight())
 })
//  49 accessing form data
$("#smbtBtn").click(function() {
  var x = $("form").serializeArray();
  $.each(x, function(i, field) {
      $("#output").append(field.name + ":"
              + field.value + " ");
  });

});
});
